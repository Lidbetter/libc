module modernc.org/libc

go 1.15

require (
	github.com/mattn/go-isatty v0.0.12
	golang.org/x/sys v0.0.0-20200826173525-f9321e4c35a6
	honnef.co/go/netdb v0.0.0-20150201073656-a416d700ae39
	modernc.org/cc/v3 v3.25.2 // indirect
	modernc.org/mathutil v1.1.1 // indirect
	modernc.org/memory v1.0.0
)
